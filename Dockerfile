FROM node:18

ENV TZ=Asia/Jerusalem
RUN echo $TZ > /etc/timezone && mkdir -p /app && apt update && apt install -y rsync ffmpeg
RUN npm install -g typescript
RUN mkdir -p /app/dist

WORKDIR /app
COPY app/package.json /app
#COPY package-lock.json /app
COPY app/loadConfig.ts /app
RUN npm install

COPY app /app
RUN npm run build

RUN mkdir -p /app/dist/Logs

CMD ["node", "--max-old-space-size=30720", "--trace-warnings", "dist/src/app.js"]
