import config from './config.main.json';
import localConfig from './config.json';

interface runApp {
    "port": number,
    "LogLevel": string
};

interface yCconfig {
    runApp: runApp,
}

const appConfig: yCconfig = Object.assign(config, localConfig);

export default appConfig;