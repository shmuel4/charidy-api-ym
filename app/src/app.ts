process.env.TZ = 'Asia/Jerusalem'
process.env["NTBA_FIX_319"] = '1';
import logger from './modules/Logger';
import EventEmitter from 'events';
import yemotRoutes from './yemot/routes';
import express from 'express';
import morgan from 'morgan';
import mainFN from './modules/mainFN';
import path from 'path';
import fs from 'fs';
import http from 'http';
import net from 'net';
import { isMainThread } from 'worker_threads';
import yCconfig from '../loadConfig'
import pkv from '../package.json';

class appManagement extends EventEmitter {
    private status: string = 'READY';
    private app = express();
    private API_SERVER: any;

    constructor() {
        super();
        if (!isMainThread) {
            logger.info(`[appManagement]: [!!!!]: constructor from not main thread, retutn`);
            return;
        }
    }

    async shutdown(): Promise<void> {
        this.API_SERVER.close();
        process.exit(99);
    }

    isReady(): boolean {
        if (this.status === 'READY') return true
        return false;
    }

    async start() {
        logger.info(`loading config From ../config.json...`);

        try {
            this.API_SERVER = this.app.listen(yCconfig.runApp.port, '0.0.0.0');
            logger.info(`yAserver app listening on port ${yCconfig.runApp.port}! packageVersion: ${pkv.version}`);
        } catch (err: any) {
            throw new Error(err)
        }

        this.app.use(express.json({ limit: '800mb' }));
        this.app.use(express.urlencoded({ limit: '800mb', extended: true }));
        morgan.token('body', (req: any) => { return mainFN.printObjectSummary(req.orginalData) });
        morgan.token('remote-addr', (req: any) => { return mainFN.getRemoteAddr(req) });
        this.app.use(morgan(':remote-addr :method :url :body :status :res[content-length] - :response-time ms', { stream: logger['yAccessLoger'] }));
        this.app.use("/yemot", (req, res, next) => mainFN.buildBody(req, res, next), yemotRoutes);
    }

}


const app = new appManagement();
app.start()

export default app;