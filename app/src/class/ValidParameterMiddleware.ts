
import { JSONObject, JSONTypeError, required, optional, passthrough, map, validate, custom, union, gt, gte, lt, lte, eq, ne, integer, array, minLength, maxLength } from "ts-json-object";
import { InvalidNumberType } from "./Errors";


class ValidParameterMiddleware extends JSONObject {

    static validInt = (object: any, key: string, value: any) => {
        if (typeof value === 'undefined') return;
        value = parseInt(value);
        if (!value || isNaN(value)) throw new InvalidNumberType(`${key} is not number`);
        return parseInt(value);
    };
}


export {
    ValidParameterMiddleware,
    JSONTypeError,
    required,
    optional,
    passthrough,
    map,
    validate,
    custom,
    union,
    gt,
    gte,
    lt,
    lte,
    eq,
    ne,
    integer,
    array,
    minLength,
    maxLength
} 