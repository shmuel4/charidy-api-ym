import logger from "../modules/Logger";
import axios from 'axios'
import mainFN from "../modules/mainFN";
import * as d2d from 'date-and-time';


export default class Charidy {
    private campaign_id: number;
    private bash_url: string = "https://api.charidy.com/api";
    private logger: DataObj['logger'];

    constructor(campaign_id: number) {
        this.campaign_id = campaign_id;
        this.logger = {
            info: function (msg: string) {
                logger.info(`[Charidy]: ${msg}`);
            },
            error: function (msg: string) {
                logger.error(`[Charidy]: ${msg}`);
            },
            warn: function (msg: string) {
                logger.warn(`[Charidy]: ${msg}`);
            },
            debug: function (msg: string) {
                logger.debug(`[Charidy]: ${msg}`);
            }
        }
    }

    private async doAPI(ws: string, body: any, wsTimeOut = 240000, method: any = "POST"): Promise<any> {
        this.logger.info(`send to ws: ${ws} in data: ${mainFN.toDataString(body)}`);
        let resStatus = -0;
        const { data } = await axios({
            method,
            data: body,
            headers: {},
            url: `${this.bash_url}/${ws}`,
            timeout: wsTimeOut,
            validateStatus: (status) => {
                resStatus = status;
                this.logger.debug(`response status code to ws: ${ws}: ${status}`)
                return true;
            }
        });
        this.logger.info(`response to ws: ${ws}: ${mainFN.toDataString(data)}`)
        return Object.assign({ resStatus }, data);
    }

    async getInfo(): Promise<ymCharidy.campaign_stats> {
        const res = await this.doAPI(`/v1/campaign/${this.campaign_id}?extend[]=campaign_stats`, {}, undefined, 'GET');
        return res;
    }

    async getTeams(): Promise<ymCharidy.teams_respones> {
        const res = await this.doAPI(`/v1/campaign/${this.campaign_id}/teams?converted_currency=1&extend_media=1&sort=-amount&parent_only=1&limit=100`, {}, undefined, 'GET');
        return res;
    }

    async getRaiser(id: number) {
        const raiser: ymCharidy.raiser_respones = await this.doAPI(`/v1/campaign/${this.campaign_id}/team/${id}`, {}, undefined, 'GET');
        if (!raiser?.data?.id) throw new Error(`bad raiser id`);
        const donors: ymCharidy.donors_respones = await this.doAPI(`/v1/campaign/${this.campaign_id}/donations?sortBy=-time&team_id=${raiser?.data?.id}&limit=10`, {}, undefined, 'GET');

        return {
            raiser,
            donors
        }
    }

    async fundraising(obj: ymCharidy.fundraising_obj) {
        const raiser = await this.getRaiser(Number(obj.raiser_id)); // מעלה קודם את הבחור שעליו מתרימים

        if (!raiser?.raiser?.data?.id) throw new Error(`raiser not exist`);

        const paymentPrms: any = await this.doAPI(`/v1/campaign/${this.campaign_id}/donation/new`, {}, undefined, 'GET');
        if (!paymentPrms?.included) throw new Error('no have included in donation/new');
        const payment_option_id = paymentPrms.included.find((p: any) => p?.type === 'payment_option' && p.attributes.name == 'kehilot')?.id;
        if (!payment_option_id) throw new Error('no have kehilot payment option');

        const postData = {
            "data": {
                "attributes": {
                    "amount": (Number(obj?.amount) * Number(obj.monthe_count)) * 100,
                    "campaign_id": this.campaign_id,
                    "payment_option_id": Number(payment_option_id),
                    "currency": "ils",
                    "currency_sign": "₪",
                    "include_payment_system_fee": false,
                    "include_payment_system_fee_amount": 0,
                    "recurring": false,
                    "recurring_period": 0,
                    "recurring_interval": "month",
                    "installment": true,
                    "installment_period": Number(obj.monthe_count),
                    "installment_interval": "month",
                    "installment_tm_option": false,
                    "billing_first_name": obj?.card_name,
                    "billing_last_name": "",
                    "billing_name": obj?.donor_name,
                    "display_name": obj?.donor_name,
                    "receipt_name": "",
                    "email": "2024+phones@tatkoltorah.org",
                    "message": obj?.dedication,
                    "team_id": Number(raiser.raiser.data.id),
                    "donate_to_all_teams": false,
                    "stream_id": 0,
                    "stream_title": "",
                    "get_data": "",
                    "address_country": "IL",
                    "address_state": "",
                    "address_city": "",
                    "address_zip": "",
                    "address_line1": "",
                    "address_number": "",
                    "address_line2": "",
                    "phone": obj?.tel,
                    "mail_receipt": false,
                    "sms_receipt": false,
                    "save_stripe_card": false,
                    "tip_amount": 0,
                    "gift_aid": false,
                    "referrer": "",
                    "captcha_token": "",
                    "lang": "",
                    "hide_amount": false,
                    "platform": "shAPI",
                    "referrer_shortlink": "tatkoltorah",
                    "zero_recurring": false,
                    "nedarimg_bit_url": "",
                    "israeli_zehut": "",
                    "pledge_ref": 0,
                    "ga": 3
                },
                "relationships": {
                    "gateway_params": {
                        "data": {
                            "type": "gateway_params",
                            "id": "0"
                        }
                    },
                    "donate_to_multiple_teams": {
                        "data": [
                            {
                                "type": "donate_to_multiple_teams",
                                "id": String(raiser.raiser.data.id)
                            }
                        ]
                    },
                    "selected_levels": {
                        "data": []
                    },
                    "custom_data": {
                        "data": []
                    }
                }
            },
            "included": [
                {
                    "type": "gateway_params",
                    "id": "0",
                    "attributes": {
                        "redirect_url": `https://donate.charidy.com/${this.campaign_id}?lang=en`
                    }
                },
                {
                    "type": "donate_to_multiple_teams",
                    "id": String(raiser.raiser.data.id),
                    "attributes": {
                        "amount": (Number(obj?.amount) * Number(obj.monthe_count)) * 100,
                        "dedication": "",
                        "name": raiser.raiser.data.attributes.name
                    }
                }
            ]
        }


        const startDonate: any = await this.doAPI(`/v1/campaign/${this.campaign_id}/donation/new`, postData, undefined, 'POST');

        //console.log('', '', ',', ',', JSON.stringify(postData), '', '', ',', ',',)

        if (!startDonate?.data?.attributes?.parameters?.url) throw new Error(`no have url to kehilot`);

        //return startDonate?.data?.attributes?.parameters?.url;

        const kehilotUrl: string = startDonate.data.attributes.parameters.url.split('?')[0];
        const payPageId = kehilotUrl.split('/')[kehilotUrl.split('/').length - 1];
        const parms = this.parseQueryString(startDonate.data.attributes.parameters.url.split('?')[1]);


        const pageInfo: ymCharidy.getPaymentPage = await this.kesherDoAPI(`/PaymentPage/getPaymentPage/${payPageId}`, parms, undefined, 'POST');

        const donePayPost = {
            "Entity": {
                "customerFieldsContent": [
                    {
                        "fieldId": 1,
                        "content": obj?.card_name
                    },
                    {
                        "fieldId": 2,
                        "content": ""
                    },
                    {
                        "fieldId": 3,
                        "content": ""
                    },
                    {
                        "fieldId": 4,
                        "content": ""
                    },
                    {
                        "fieldId": 5,
                        "content": obj?.tel?.substring(2, obj?.tel?.length)
                    },
                    {
                        "fieldId": 8,
                        "content": "2024+phones@tatkoltorah.org"
                    },
                    {
                        "fieldId": 9,
                        "content": " "
                    },
                    {
                        "fieldId": 15,
                        "content": `תרומה מספר ${startDonate?.data?.attributes?.donation_id} בקמפיין אחים אנחנו שהושלמה דרך הטלפון`
                    }
                ],
                "paymentFieldsDetails": {
                    "sum": Number(obj?.amount),
                    "currency": {
                        "id": 1,
                        "symbol": "₪",
                        "isLinkageCurrency": false,
                        "isActive": null,
                        "value": "1"
                    },
                    "numberPayment": obj?.monthe_count,
                    "transactionDate": d2d.format(new Date(), 'DD/MM/YYYY'),
                    "expiry": obj?.exp_date,
                    "cvv": obj?.cvv,
                    "creditCard": obj?.card_number
                },
                "documentDetails": {
                    "documentName": obj?.card_name
                },
                "products": [],
                "paymentType": Number(obj.monthe_count) > 1 ? 3 : 1,
                "captchaToken": null,
                "paymentPageId": payPageId,
                "guid": pageInfo.guid
            }
        }

        const donateDone: ymCharidy.submitTransaction = await this.kesherDoAPI(`/PaymentPage/submitTransaction`, donePayPost, undefined, 'POST');
        if (donateDone?.url) {
            await axios({
                method: 'GET',
                headers: {},
                url: donateDone?.url,
                validateStatus: (status) => {
                    return true;
                }
            });
        }

        return donateDone;
    }

    private async kesherDoAPI(ws: string, body: any, wsTimeOut = 240000, method: any = "POST") {
        //https://api2022.kesherhk.info/api/PaymentPage/getPaymentPage/314966
        this.logger.info(`send to ws: ${ws} in data: ${mainFN.toDataString(body)}`);
        let resStatus = -0;
        const { data } = await axios({
            method,
            data: body,
            headers: {},
            url: `https://api2022.kesherhk.info/api${ws}`,
            timeout: wsTimeOut,
            validateStatus: (status) => {
                resStatus = status;
                this.logger.debug(`response status code to ws: ${ws}: ${status}`)
                return true;
            }
        });
        this.logger.info(`response to ws: ${ws}: ${mainFN.toDataString(data)}`)
        return Object.assign({ resStatus }, data);
    }

    private parseQueryString(query: string) {
        const uri = new URLSearchParams(query);
        const params: any = {};
        for (const _kv of uri) {
            params[_kv[0]] = _kv[1];
        }
        return params;
    }
}
