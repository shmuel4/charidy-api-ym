process.env.TZ = 'Asia/Jerusalem'
import path from 'path';
import yCconfig from '../../loadConfig';
import { workerData, isMainThread } from 'worker_threads';
import { createLogger, format, transports } from 'winston';
const { combine, timestamp, label, printf } = format;
let isCli: boolean = false;
const myFormat = printf(({ level, message, label, timestamp }) => {
    return `[${timestamp}] [${isMainThread ? 'MAIN' : workerData.id}] [${label}] ${level.toUpperCase()}: ${message}`;
});

const logger: any = createLogger({
    format: combine(
        label({ label: yCconfig.runApp.LogLevel }),
        timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
        myFormat,
    ),
    level: yCconfig.runApp.LogLevel,
    transports: [new transports.Console(), new transports.File({
        filename: path.join(__dirname, '../../Logs/yAlog.log'),
        maxsize: 100000000,
        maxFiles: 25,
    })]
});

logger['yAccessLoger'] = {
    write(message: string) {
        logger.info(message);
    }
};

export default logger;
