import logger from './Logger';
import yCconfig from '../../loadConfig';
import pkv from '../../package.json';
import axios from 'axios';
import fs from 'fs';

const checkRequiredValues = (vals: string[] = [], data: any) => {
    if (!data) throw new Error(`Validation - No master key exists`);
    for (let key of vals) {
        if (!data[key]) {
            throw {
                "message": "Missing parameters",
                "parameter": key
            }
        }
    }
    return;
}

const isAllowValue = (vals: string[] = [], data: object) => {
    for (let key of Object.keys(data)) {
        if (!vals.includes(key)) throw new Error(`key '${key}' not Allow`);
    }
    return;
}

const isAllowString = (vals: string[] = [], data: string) => {
    if (!vals.includes(data)) throw new Error(`value '${data}' not Allow`);
    return;
}

const sql_date = function () {
    let d = new Date(Date.now());
    return d.getFullYear() + "-" + ("00" + (d.getMonth() + 1)).slice(-2) + "-" + ("00" + d.getDate()).slice(-2) + " " + ("00" + d.getHours()).slice(-2) + ":" + ("00" + d.getMinutes()).slice(-2) + ":" + ("00" + d.getSeconds()).slice(-2);
}

const get_date = function (date: Date) {
    if (!date) {
        return '';
    }
    let d = new Date(date);
    return d.getFullYear() + "-" + ("00" + (d.getMonth() + 1)).slice(-2) + "-" + ("00" + d.getDate()).slice(-2) + " " + ("00" + d.getHours()).slice(-2) + ":" + ("00" + d.getMinutes()).slice(-2) + ":" + ("00" + d.getSeconds()).slice(-2);
}

const toDataString = (obj: any) => {
    if (!obj || typeof obj !== "object") {
        return obj;
    }
    let string = '[';
    for (const key of Object.keys(obj)) {
        if (obj[key] && typeof obj[key] === "object") {
            string += `${key}=${toDataString(obj[key])};`
            continue;
        }
        string += `${key}=${obj[key]};`
    }
    string += ']';
    return string
}

const diff_data = (date1: string, date2: string) => {
    const Date1 = new Date(date1).getTime();
    const Date2 = new Date(date2).getTime();
    const diff_ms = Math.abs(Date2 - Date1);
    const diff_days = Math.ceil(diff_ms / (1000 * 60 * 60 * 24));
    return { diff_ms, diff_days };
}

const validInt = (value: any) => {
    value = parseInt(value);
    if (!value || isNaN(value)) throw new Error('חיפוש זה מאפשר לחפש מספרים בלבד');
    return value;
}

const validSrting = (value: any) => {
    value = value?.replaceAll("%", "");
    if (!value) throw new Error('חובה לחפש משהוא...');
    return value;
}

const validBool = (value: any) => {
    if (typeof value === 'number') return Boolean(Number(value));
    return Boolean(value);
}

const getRandomInt = (min: number, max: number) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const printObjectSummary = (obj: any) => {
    const notPrintKeys = ['token', 'password'];
    try {
        if (!obj || typeof obj !== 'object') return;
        let string = '';

        // Set default value for indentation
        // Iterate through object properties
        for (const key of Object.keys(obj)) {
            let value = obj[key];

            // Print key and indentation
            //console.log(" ".repeat(indent) + key + ": ");
            if (typeof value === 'function' || key === 'logger') continue;
            if (notPrintKeys.includes(key)) {
                if (typeof value === 'string') {
                    value = `${value.substring(0, 4)}...${value.substring(value.length - 4)}`;
                } else {
                    value = 'hidden';
                }
            }
            string += `[${key}: `;

            // Print value
            if (typeof value === "object" && value != null) {
                // If value is an object, print summary of object
                let valueToSend = value;
                if (Array.isArray(valueToSend)) {
                    string += printObjectSummary(valueToSend.splice(0, 10)) + ` (size: ${valueToSend.length})...] `;
                } else {
                    if (Object.keys(valueToSend).length > 10) {
                        let miniValueToSend: any = {};
                        for (const _k of Object.keys(valueToSend)) {
                            if (Object.keys(miniValueToSend).length >= 10) break;
                            miniValueToSend[_k] = valueToSend[_k];
                        }
                        string += printObjectSummary(miniValueToSend) + ` (size: ${Object.keys(valueToSend).length})...] `;
                    } else {
                        string += printObjectSummary(valueToSend) + "] ";
                    }
                }
            } else {
                // If value is not an object, print value as string
                if (String(value).length > 20) {
                    string += String(value).substring(0, 20) + "...] ";
                } else {
                    string += String(value) + "] ";
                }
            }
        }
        return string;
    } catch (e: any) {
        logger.warn(`error on printObjectSummary: ${e.message}`);
        console.log(e);
        console.log(obj);
    }
}

const getRemoteAddr = (req: any) => {
    return req.headers['x-forwarded-for'] || req.connection.remoteAddress;
}

const sleep = (ms: number) => {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

const isProduction = () => {
    let isProduction = false;
    process.env.PRODUCTION ? isProduction = true : isProduction = false;
    return isProduction;
}

const jsonResponseError = (mess: string, data: any, res: any): any => {
    if (res.isFake === true) return {
        "responseStatus": "ERROR",
        "message": mess,
        "apiVersion": pkv.version
    };
    res.json({
        "responseStatus": "ERROR",
        "message": mess,
        "apiVersion": pkv.version
    })
}

const jsonResponseException = (mess: any, data: DataObj, res: ymExpress.Response) => {
    const exceptionClass = mess?.constructor?.name || 'unknown';
    if (exceptionClass === 'InvalidWSParameters') {
        logger.warn(`Exception! [InvalidWSParameters]: ${toDataString(mess?.getZodError()?.issues)}`);
        return res.json({
            "responseStatus": "Exception",
            exceptionClass,
            "message": mess.message,
            issues: mess.getZodError()?.issues
        });
    }
    let fileName: any;
    let file: any;
    if (mess.demeError) {
        if (res.isFake === true) return {
            "responseStatus": "ERROR",
            exceptionClass,
            "message": mess.me,
        };
        return res.json({
            "responseStatus": "ERROR",
            exceptionClass,
            "message": mess.me,
        });
    }
    try {
        if (mess.stack) {
            logger.warn(`Exception! ${mess.stack}`)
            file = mess.stack.split('\n')[1].split('/');
            file = file[file.length - 1].substring(0, file[file.length - 1].length - 1);
            file = file.split(':');
            fileName = file[0];
        } else {
            logger.warn(`Exception! ${mess.message} in ${mess.stack}`);
            //console.log(mess);
        }

        let Err = {
            "responseStatus": "Exception",
            exceptionClass,
            "apiVersion": pkv.version
        }

        if (typeof mess === 'object' && mess.parameter) {
            Err = Object.assign(Err, mess)
            if (res.isFake === true) return Err;
            res.json(Err);
        } else {
            const errRe = {
                "responseStatus": "Exception",
                exceptionClass,
                "message": mess.message ? mess.message : mess,
                "file": `${fileName.substring(0, fileName.length - 3)}.cellular`,
                "line": (file ? `${file[1]}:${file[2]}` : null),
                "apiVersion": pkv.version
            };
            if (res.isFake === true) return errRe;
            res.json(errRe);
        }
    } catch (e) {
        const errRe = {
            "responseStatus": "Exception",
            exceptionClass,
            "message": mess.message ? mess.message : mess,
            "file": fileName,
            "line": (file ? `${file[1]}:${file[2]}` : null),
            "apiVersion": pkv.version
        };
        if (res.isFake === true) return errRe;
        res.json(errRe);
    }
}

const jsonResponseOK = (mess: any, data: DataObj, res: ymExpress.Response) => {
    let json = {
        "responseStatus": "OK",
        apiVersion: pkv.version
    }
    if (res.isFake === true) return Object.assign(json, mess);
    res.status(200).json(Object.assign(json, mess));
}

const buildBody = async (req: any, res: any, next: any) => {
    const remoteIP = req.connection.remoteAddress;
    let data;
    if (req.method === "POST") {
        data = req.body;
        Object.assign(data, req.query);
    } else {
        data = req.query;
    }
    let myLoger = {
        info: function (msg: string) {
            logger.info(`[${remoteIP}] [${req._parsedUrl.pathname}] ${msg}`);
        },
        error: function (msg: string) {
            logger.error(`[${remoteIP}] [${req._parsedUrl.pathname}] ${msg}`);
        },
        warn: function (msg: string) {
            logger.warn(`[${remoteIP}] [${req._parsedUrl.pathname}] ${msg}`);
        }
    }

    data.logger = myLoger;
    data.responseStatus = "responseStatus";
    data.SystemMode = "mohMonitor";
    data.userType = "mohMonitor";
    req.yAP = data;
    next();
}

export default {
    buildBody,
    jsonResponseOK,
    jsonResponseException,
    jsonResponseError,
    validInt,
    getRandomInt,
    validSrting,
    validBool,
    diff_data,
    checkRequiredValues,
    sql_date,
    get_date,
    toDataString,
    isAllowValue,
    isAllowString,
    printObjectSummary,
    getRemoteAddr,
    sleep,
    isProduction,
};