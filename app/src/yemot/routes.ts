import * as express from 'express';
const router = express.Router();
import mainFN from '../modules/mainFN';

import campaignInfo from './webServices/info';
router.get('/:campaign_id/info', (req: any, res: any) => { campaignInfo(req, res) });


import campaignTeams from './webServices/teams';
router.get('/:campaign_id/teams', (req: any, res: any) => { campaignTeams(req, res) });


import campaignRaiser from './webServices/raiser';
router.get('/:campaign_id/:raiser_id/raiser', (req: any, res: any) => { campaignRaiser(req, res) });


import campaignFundraising from './webServices/fundraising';
router.post('/:campaign_id/fundraising', (req: any, res: any) => { campaignFundraising(req, res) });


router.all('*', (req: any, res: any) => { mainFN.jsonResponseError('BAD_REQUEST', req.yAP, res) });
export default router;