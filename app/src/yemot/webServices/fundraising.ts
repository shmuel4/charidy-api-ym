import mainFN from "../../modules/mainFN";
import { ValidParameterMiddleware, required, optional, union, custom } from "../../class/ValidParameterMiddleware";
import Charidy from "../../class/charidy";

class WebServiesParameters extends ValidParameterMiddleware {
    @required card_number!: string;
    @required exp_date!: string;
    @required cvv!: string;
    @required card_name!: string;
    @required donor_name!: string;
    @required raiser_id!: string;
    @required monthe_count!: string;
    @required tel!: string;
    @required amount!: string;
    @required dedication!: string;
}

class RouteServiesParameters extends ValidParameterMiddleware {
    @custom(ValidParameterMiddleware.validInt) campaign_id!: number;
    @custom(ValidParameterMiddleware.validInt) raiser_id!: number;
}

export default async (req: ymExpress.Request, res: ymExpress.Response) => {
    const data: DataObj = req.yAP;

    try {
        const prms = new WebServiesParameters(data['obj']);
        const routePrms = new RouteServiesParameters(req.params);
        const charidy = new Charidy(routePrms.campaign_id);

        const campaign_raiser = await charidy.fundraising(prms);
        mainFN.jsonResponseOK({ campaign_raiser }, data, res);
    } catch (e) {
        mainFN.jsonResponseException(e, data, res);
    }
}