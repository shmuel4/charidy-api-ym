import mainFN from "../../modules/mainFN";
import { ValidParameterMiddleware, required, optional, union, custom } from "../../class/ValidParameterMiddleware";
import Charidy from "../../class/charidy";


class RouteServiesParameters extends ValidParameterMiddleware {
    @custom(ValidParameterMiddleware.validInt) campaign_id!: number;
}

export default async (req: ymExpress.Request, res: ymExpress.Response) => {
    const data: DataObj = req.yAP;

    try {
        const routePrms = new RouteServiesParameters(req.params);
        const charidy = new Charidy(routePrms.campaign_id);

        const campaign_info = await charidy.getInfo();

        const viweRes: any = {}
        viweRes.total = (campaign_info.data.relationships.campaign_stats.data.total)?.toFixed();
        viweRes.dist = (campaign_info.data.attributes.rounds?.[0]?.goal || 0)?.toFixed();
        viweRes.percent = ((viweRes.total / viweRes.dist) * 100)?.toFixed();

        mainFN.jsonResponseOK(viweRes, data, res);
    } catch (e) {
        mainFN.jsonResponseException(e, data, res);
    }
}