import mainFN from "../../modules/mainFN";
import { ValidParameterMiddleware, required, optional, union, custom } from "../../class/ValidParameterMiddleware";
import Charidy from "../../class/charidy";


class RouteServiesParameters extends ValidParameterMiddleware {
    @custom(ValidParameterMiddleware.validInt) campaign_id!: number;
    @custom(ValidParameterMiddleware.validInt) raiser_id!: number;
}

export default async (req: ymExpress.Request, res: ymExpress.Response) => {
    const data: DataObj = req.yAP;

    try {
        const routePrms = new RouteServiesParameters(req.params);
        const charidy = new Charidy(routePrms.campaign_id);

        const campaign_raiser = await charidy.getRaiser(routePrms.raiser_id);

        const viweRes: any = {
            id: campaign_raiser.raiser?.data?.id,
            name: campaign_raiser.raiser?.data?.attributes?.name,
            total: (campaign_raiser.raiser?.data?.attributes?.donated).toFixed(),
            dist: (campaign_raiser.raiser?.data?.attributes?.goal).toFixed(),
            donors: []
        };

        campaign_raiser?.donors?.data?.forEach((d) => {
            viweRes.donors.push({
                name: d?.attributes?.name,
                amount: (d?.attributes?.total).toFixed()
            })
        });

        mainFN.jsonResponseOK(viweRes, data, res);
    } catch (e) {
        mainFN.jsonResponseException(e, data, res);
    }
}