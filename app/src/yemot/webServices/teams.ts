import mainFN from "../../modules/mainFN";
import { ValidParameterMiddleware, required, optional, union, custom } from "../../class/ValidParameterMiddleware";
import Charidy from "../../class/charidy";


class RouteServiesParameters extends ValidParameterMiddleware {
    @custom(ValidParameterMiddleware.validInt) campaign_id!: number;
}

export default async (req: ymExpress.Request, res: ymExpress.Response) => {
    const data: DataObj = req.yAP;

    try {
        const routePrms = new RouteServiesParameters(req.params);
        const charidy = new Charidy(routePrms.campaign_id);

        const campaign_teams = await charidy.getTeams();

        const viweRes: any = [];


        campaign_teams.data.forEach((t) => {
            viweRes.push({
                id: t.id,
                name: t.attributes.name,
                total: (t.attributes.donated + t.attributes.donated_children).toFixed(),
                dist: (t.attributes.goal).toFixed(),
            })
        });


        mainFN.jsonResponseOK({ teams: viweRes }, data, res);
    } catch (e) {
        mainFN.jsonResponseException(e, data, res);
    }
}