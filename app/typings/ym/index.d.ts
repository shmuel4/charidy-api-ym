/*
* B.H.
*/

import { Logger } from "winston"

declare global {
    namespace ymExpress {
        interface Request extends Express.Request {
            headers: object;
            connection: { remoteAddress: string },
            method: string,
            body: object,
            query: object,
            _parsedUrl: { pathname: string },
            yAP: any,
            params: any
            orginalData?: any
        }
        interface Response extends Express.Response {
            status(code: number): Response,
            json(jsonData: object): void,
            header(key: string, value: string): void
            download(filePath: string, fileName?: string): void
            send(body: string | Buffer): void
            isFake: boolean,
        }
    }

    interface ResponseError {
        "responseStatus": string,
        "message": string,
        "apiVersion": string
    }

    interface DataObj {
        logger: { info(mes: string): void, error(mes: string): void, warn(mes: string): void, debug(mes: string): void },
        obj: any
    }

    interface Exception {
        demeError: boolean
        me: string
        stack: string
        message: string
        parameter: string
    }

}