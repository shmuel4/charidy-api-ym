import ym from "./index"


declare global {
    namespace ymCharidy {

        interface campaign_stats {
            data: {
                type: "campaign",
                id: number,
                attributes: {
                    campaign_id: number,
                    currency: "ils",
                    currency_sign: "₪",
                    about_campaign: "",
                    short_link: string,
                    start_date: string,
                    end_date: string,
                    campaign_video: string,
                    campaign_image: string,
                    campaign_extra_images: [],
                    rounds:
                    {
                        round: number,
                        goal: number,
                        multiplier: number,
                        label: string
                    }[]
                    ,
                    title: string,
                    short_description: string,
                    campaign_mobile_image: string,
                    mode: number,
                    type: number,
                    theme: string,
                    category: "regular",
                    givingday_id: number,
                    text_to_pledge_number: string,
                    currencies:
                    {
                        code: "ILS",
                        sign: "₪"
                    }[]
                },
                relationships: {
                    campaign_stats: {
                        data: {
                            total: number,
                            donors_total: number,
                            total_campaigns: number,
                            real: number,
                            real_matched: num,
                            extra_data: {}
                        }
                    }
                }
            }
        }


        interface ConvertedCurrency {
            currency: string;
            currency_sign: string;
            goal: number;
            original_goal: number;
            donated: number;
            donated_children: number;
        }

        interface Attributes {
            campaign_currency: string;
            campaign_id: number;
            campaign_shortlink: string;
            child_teams_count: number;
            color: string;
            converted_currency: ConvertedCurrency[];
            custom_fields: Record<string, unknown>;
            custom_link: string;
            description: string;
            display_goal_currency: string;
            donated: number;
            donated_children: number;
            donated_real: number;
            donation_n: number;
            donation_n_children: number;
            donor_goal: number;
            ex: null;
            goal: number;
            goal_categories: string;
            grandchild_teams_count: number;
            group: string;
            hidden: boolean;
            hide_goal_and_progress: boolean;
            image: string;
            is_parent: boolean;
            join_as: string;
            name: string;
            original_goal: number;
            parent_level: number;
            parent_team_id: number;
            parent_team_name: string;
            parent_team_slug: string;
            percentage_view: boolean;
            sefer_torah: null;
            shortlink: string;
            slug: string;
            sort: number;
        }

        interface RelationshipData {
            data: any[];
        }

        interface Relationships {
            levels: RelationshipData;
            media: RelationshipData;
        }

        interface Team {
            type: string;
            id: string;
            attributes: Attributes;
            relationships: Relationships;
        }

        interface teams_respones {
            data: Team[];
        }

        interface raiser_respones {
            data: {
                type: string;
                id: string;
                attributes: {
                    campaign_currency: string;
                    campaign_id: number;
                    campaign_shortlink: string;
                    child_teams_count: number;
                    color: string;
                    converted_currency: any[];
                    custom_fields: object;
                    custom_link: string;
                    description: string;
                    display_goal_currency: string;
                    donated: number;
                    donated_children: number;
                    donated_real: number;
                    donation_n: number;
                    donation_n_children: number;
                    donor_goal: number;
                    ex: null;
                    goal: number;
                    goal_categories: string;
                    grandchild_teams_count: number;
                    group: string;
                    hidden: boolean;
                    hide_goal_and_progress: boolean;
                    image: string;
                    is_parent: boolean;
                    join_as: string;
                    name: string;
                    original_goal: number;
                    parent_level: number;
                    parent_team_id: number;
                    parent_team_name: string;
                    parent_team_slug: string;
                    percentage_view: boolean;
                    sefer_torah: null;
                    shortlink: string;
                    slug: string;
                    sort: number;
                };
                relationships: {
                    levels: {
                        data: any[];
                    };
                    media: {
                        data: any[];
                    };
                };
            };
        }

        interface Donation {
            type: string;
            id: string;
            attributes: DonationAttributes;
            relationships: {
                team_list: {
                    data: any[]; // משתנה לפי המבנה המדויק של הנתונים בתוך המערך
                };
            };
        }

        interface DonationAttributes {
            campaign: CampaignInfo;
            campaign_id: number;
            category: string;
            charge_currency_code: string;
            charge_currency_sign: string;
            converted_currency: null; // יש לעדכן את הטיפוס בהתאם למבנה המדויק של הנתונים
            covered_processing_fee: boolean;
            created_at: number;
            currency_code: string;
            currency_sign: string;
            custom_field_1: string;
            custom_field_2: string;
            dedication: string;
            donated_with_teams_count: number;
            donation_level: null; // יש לעדכן את הטיפוס בהתאם למבנה המדויק של הנתונים
            donor: Donor;
            donor_stats: null; // יש לעדכן את הטיפוס בהתאם למבנה המדויק של הנתונים
            earmark: string;
            ex: null; // יש לעדכן את הטיפוס בהתאם למבנה המדויק של הנתונים
            gateway_name: string;
            name: string;
            org_comment: string;
            organization: null; // יש לעדכן את הטיפוס בהתאם למבנה המדויק של הנתונים
            real_payment: number;
            selected_levels: any[]; // משתנה לפי המבנה המדויק של הנתונים בתוך המערך
            subscription_duration: number;
            subscription_interval: string;
            subscription_month_amount: number;
            team: null; // יש לעדכן את הטיפוס בהתאם למבנה המדויק של הנתונים
            team_id: number;
            team_id_list: number[];
            team_id_to_total_charge_currency: { [key: number]: number };
            tipped: boolean;
            total: number;
            total_charge_currency: number;
        }

        interface CampaignInfo {
            campaign_id: number;
            currency: string;
            currency_sign: string;
            about_campaign: string;
            short_link: string;
            start_date: string;
            end_date: string;
            campaign_video: string;
            campaign_image: string;
            campaign_extra_images: null; // יש לעדכן את הטיפוס בהתאם למבנה המדויק של הנתונים
            rounds: null; // יש לעדכן את הטיפוס בהתאם למבנה המדויק של הנתונים
            title: string;
            short_description: string;
            campaign_mobile_image: string;
            mode: number;
            type: number;
            theme: string;
            category: string;
            givingday_id: number;
            text_to_pledge_number: string;
            currencies: null; // יש לעדכן את הטיפוס בהתאם למבנה המדויק של הנתונים
        }

        interface Donor {
            id: number;
            anonymous_donation: boolean;
            avatar: string;
        }

        interface donors_respones {
            data: Donation[];
        }

        interface fundraising_obj {
            'card_number': string,
            'exp_date': string,
            'cvv': string,
            'card_name': string,
            'donor_name': string,
            'raiser_id': string,
            'monthe_count': string,
            'dedication': string,
            'tel': string,
            'amount': string,
        }

        interface getPaymentPage {
            pageType: number;
            iframeType: number;
            products: any[];
            categoriesAndProductsRoot: string;
            packagesDetails: PackagesDetails;
            signatureExists: boolean;
            kehilotParameters: any;
            campaignDetails: any;
            dynamicCustomerFields: any[];
            dynamicPaymentFields: any[];
            clubsSettings: any;
            guid: string;
            token: string;
            header: Header;
            paymentFields: PaymentField[];
            settings: Settings;
            currencies: Currency[];
            clientId: any;
            policySiteCompanyDetails: PolicySiteCompanyDetails;
            id: number;
            paymentTypes: PaymentType[];
            customerFields: CustomerField[];
            response: Response;
        }

        interface PackagesDetails {
            packages: any[];
            package: any;
        }

        interface Header {
            showShareButton: boolean;
            shareButtonText: any;
            adUrl: any;
            name: string;
            description: any;
            logoPath: string;
            pageType: number;
            defaultLanguage: any;
            startDate: any;
            endDate: any;
        }

        interface PaymentField {
            fieldId: number;
            title: string;
            fieldType: number;
            fieldOption: number;
            autoComplete: any;
            index: any;
            defaultValue: any;
        }

        interface Settings {
            allowsCurrencyLinkage: boolean;
            displayPaymentSectionFirst: boolean;
            defaultCurrency: number;
            commentLabel: any;
            defaultComment: any;
            defaultLanguage: number;
            startDate: any;
            endDate: any;
            submitButtonText: any;
            showRecaptcha: boolean;
            isRegisteredForDocumentProduction: boolean;
            receiptNameDefaultValue: string;
            showSwipeCardButton: boolean;
            hideTotalDetails: boolean;
            displayInKehilot: any;
            labelKehilotId: any;
            allowRegisteredUser: boolean;
            allowDifferentNameForDocument: boolean;
            showSiteRregulations: boolean;
            defaultSum: number;
            minSum: any;
            maxSum: any;
            defaultNumPayment: number;
            maxNumPayment: any;
            disableChangeNumberPayments: boolean;
        }

        interface Currency {
            id: number;
            symbol: string;
            isLinkageCurrency: boolean;
            isActive: any;
            value: string;
        }

        interface PolicySiteCompanyDetails {
            companyName: string;
            numAssociation: string;
            address: string;
            phone: string;
        }

        interface PaymentType {
            typeId: number;
            name: string;
            creditValue: string;
            description: string;
            img: string;
            index: number;
            inactive: any;
        }

        interface CustomerField {
            fieldId: number;
            title: string;
            fieldType: number;
            fieldOption: number;
            autoComplete: string;
            index: any;
            defaultValue: string;
        }

        interface Response {
            status: number;
            succeeded: boolean;
            errorMessage: any;
        }

        interface submitTransaction {
            "errorCode": string,
            "url": string,
            "customText": string,
            "confirmNumber": string,
            "obligationId": string,
            "isBitUrl": string,
            "transactionToken": string,
            "succeeded": string,
            "errorMessage": string,
            "status": number
        }

    }



}