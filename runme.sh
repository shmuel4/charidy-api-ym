#!/bin/bash
#
# B.H.
#

script=`readlink -f $0`
dir=`dirname $script`
project=`basename $dir`
cd $dir

docker stop $project
docker rm $project

docker run -d --restart always \
	-v $dir/Logs:/app/dist/Logs \
	-p 1666:1666 --name $project $project
